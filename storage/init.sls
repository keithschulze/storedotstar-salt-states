include:
  - storage.network_mounts

# Set up the zfs filesystem if a volume is attached
{% if 0 == salt['cmd.retcode']('test -b /dev/vdc') %}
zfs-repo:
  pkgrepo.managed:
    - ppa: zfs-native/stable

ubuntu-zfs:
  pkg.installed:
    - require:
        - pkgrepo: zfs-repo

zfs-kernel-module:
  kmod.present:
    - name: zfs
    - require:
        - pkg: ubuntu-zfs
          
zfs-filesystem:
  module.run:
    - name: zpool.create
    - pool_name: zfs_volume_storage
    - zpool: zfs_volume_storage
    - vdevs:
      - /dev/vdc
    - kwargs:
        force: True
    - require:
        - pkg: ubuntu-zfs
        - kmod: zfs-kernel-module

/var/lib/tardis_storage:
  file.symlink:
    - target: /zfs_volume_storage
    - require:
        - module: zfs-filesystem
# Just create a directory locally if no volume is attached
{% else %}
/var/lib/tardis_storage:
  file.symlink:
    - target: /mnt
{% endif %}