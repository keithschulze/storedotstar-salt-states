
{% set munin_dir = salt['pillar.get']('logging:munin_dir', '/mnt/munin') %}

munin-apache2:
  pkg:
    - name: apache2
    - installed
  service.running:
    - name: apache2
    - watch:
        - file: /etc/munin/apache.conf
        - file: /etc/munin/munin.conf
        - file: munin-apache2-ports
        - file: /etc/apache2/conf-enabled/munin.conf

munin-apache2-ports:
  file.managed:
    - name: /etc/apache2/ports.conf
    - source: salt://logging/templates/apache2-ports.conf
    - require:
        - pkg: munin-apache2

munin-apache2-vhost:
  file.managed:
    - name: /etc/apache2/sites-available/000-default.conf
    - source: salt://logging/templates/apache2-vhost.conf

munin-apache2-utils:
  pkg:
    - name: apache2-utils
    - installed

libcgi-fast-perl:
  pkg:
    - installed
    - require:
        - pkg: munin-apache2

libapache2-mod-fcgid:
  pkg:
    - installed
    - require:
        - pkg: munin-apache2

fcgid:
  apache_module:
    - enable
    - require:
        - pkg: libapache2-mod-fcgid
        - pkg: munin-apache2
        - pkg: munin-apache2-utils

munin:
  pkg:
    - installed
    - require:
        - pkg: munin-apache2
        - pkg: munin-apache2-utils
        - apache_module: fcgid
  service.running:
    - watch:
        - file: /etc/munin/munin.conf



{{ munin_dir }}:
  file.directory:
    - user: munin
    - group: munin
    - require:
        - pkg: munin

/etc/munin/apache.conf:
  file:
    - managed
    - source: salt://logging/templates/munin-apache.conf
    - template: jinja
    - defaults:
        munin_dir: {{ munin_dir }}
    - require:
        - pkg: munin

/etc/munin/munin.conf:
  file:
    - managed
    - source: salt://logging/templates/munin.conf
    - template: jinja
    - defaults:
        munin_dir: {{ munin_dir }}
    - require:
        - pkg: munin
        - file: {{ munin_dir }}


/etc/apache2/conf-available/munin.conf:
  file.symlink:
    - target: /etc/munin/apache.conf
    - require:
        - file: /etc/munin/apache.conf

/etc/apache2/conf-enabled/munin.conf:
  file.symlink:
    - target: ../conf-available/munin.conf
    - require:
        - file: /etc/apache2/conf-available/munin.conf
