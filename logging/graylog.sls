include:
  - storage
  - common.oracle-java
  
# Setup logging directory
/var/lib/tardis_storage/logging_db:
  file.directory:
    - user: root
    - group: root
    - mode: 775
    - makedirs: True
    - require:
        - sls: storage

/logging_db:
  file.symlink:
    - target: /var/lib/tardis_storage/logging_db
    - require:
        - file: /var/lib/tardis_storage/logging_db

elasticsearch-repo:
  pkgrepo.managed:
    - key_url: http://packages.elasticsearch.org/GPG-KEY-elasticsearch
    - name: deb http://packages.elasticsearch.org/elasticsearch/1.7/debian stable main
    - require_in:
      - pkg: elasticsearch

elasticsearch:
  pkg:
    - latest
    - refresh: True
    - require:
      - pkg: java
  service.running:
    - enable: true
    - watch:
      - file: /etc/elasticsearch/elasticsearch.yml

elasticsearch_data_dir:
  file:
    - name: /logging_db/elasticsearch/data
    - directory
    - user: elasticsearch
    - group: root
    - makedirs: true
    - require:
      - pkg: elasticsearch

elasticsearch_work_dir:
  file:
    - name: /logging_db/elasticsearch/work
    - directory
    - user: elasticsearch
    - group: root
    - makedirs: true
    - require:
      - pkg: elasticsearch

/etc/elasticsearch/elasticsearch.yml:
  file:
    - managed
    - source: salt://logging/templates/elasticsearch.yml
    - require:
      - file: elasticsearch_data_dir
      - file: elasticsearch_work_dir

/tmp/graylog-1.3-repository-ubuntu14.04_latest.deb:
  file:
    - managed
    - source: salt://logging/packages/graylog-1.3-repository-ubuntu14.04_latest.deb

apt-transport-https:
  pkg:
    - installed

mongodb-org-keys:
  cmd.run:
    - name: apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
    - unless: apt-key list | grep -q 7F0CEB10

mongodb-org-repo:
  pkgrepo.managed:
    - name: deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse
    - require:
      - cmd: mongodb-org-keys
    - require_in:
      - pkg: mongodb-org

mongodb-org:
  pkg:
    - installed

mongo_data_dir:
  file:
    - name: /logging_db/mongodb
    - user: mongodb
    - group: mongodb
    - directory
    - require:
      - pkg: mongodb-org

/etc/mongod.conf:
  file:
    - managed
    - source: salt://logging/templates/mongod.conf
    - require:
      - pkg: mongodb-org

mongod:
  service.running:
    - require:
      - pkg: mongodb-org
    - watch:
      - file: mongo_data_dir
      - file: /etc/mongod.conf

# Remove old graylog packages if exists
graylog-1.2-repository-ubuntu12.04:
  pkg:
    - purged
    - require_in:
        - cmd: install-graylog
        
install-graylog:
  cmd.run:
    - name: dpkg -i /tmp/graylog-1.3-repository-ubuntu14.04_latest.deb
    - require:
      - file: /tmp/graylog-1.3-repository-ubuntu14.04_latest.deb
      - pkg: apt-transport-https
    - unless: dpkg -s graylog-1.3-repository-ubuntu14.04

graylog-server-config:
  file:
    - name: /etc/graylog/server/server.conf
    - managed
    - source: salt://logging/templates/server.conf
    - template: jinja
    - defaults:
        secret: {{ pillar['graylog']['secret'] }}
        admin_pwd_sha256: {{ pillar['graylog']['server']['admin_pwd_sha256'] }}
    - require:
      - pkg: graylog-pkgs

graylog-web-config:
  file:
    - name: /etc/graylog/web/web.conf
    - managed
    - source: salt://logging/templates/web.conf
    - template: jinja
    - defaults:
        secret: {{ pillar['graylog']['secret'] }}
    - require:
      - pkg: graylog-pkgs

graylog-pkgs:
  pkg:
    - latest
    - refresh: true
    - names:
      - graylog-server
      - graylog-web
    - require:
      - cmd: install-graylog
      - pkg: java
      - pkg: mongodb-org

graylog-server:
  service.running:
    - enable: true
    - require:
      - pkg: graylog-pkgs
      - file: graylog-server-config
    - watch:
      - file: graylog-server-config

graylog-web:
  service.running:
    - enable: true
    - require:
      - pkg: graylog-pkgs
      - file: graylog-web-config
    - watch:
      - file: graylog-web-config
      - pkg: graylog-pkgs

{% if salt['pillar.get']('rsyslog:server-cert', None) %}
/etc/rsyslog_server.pem:
  file:
    - managed
    - contents_pillar: rsyslog:server-cert
    - mode: 644
    - user: root
    - group: root

/etc/rsyslog_server_key.pem:
  file:
    - managed
    - contents_pillar: rsyslog:server-pkey
    - mode: 640
    - user: graylog
    - group: root
    - require:
        - pkg: graylog-pkgs
{% endif %}