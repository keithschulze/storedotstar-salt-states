include:
  - networking.firewall.default

salt:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 4505:4506
    - proto: tcp
    - save: True
    - require:
      - sls: networking.firewall.default