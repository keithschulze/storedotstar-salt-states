include:
  - networking.firewall.default

mydata_scp:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 2201
    - proto: tcp
    - save: True
    - require:
      - sls: networking.firewall.default
