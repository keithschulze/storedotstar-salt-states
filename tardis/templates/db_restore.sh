#!/bin/bash

LOG="/home/mytardis/db_restore.log"

PGPASSWORD=$TARDIS_DB_PASSWORD pg_restore --clean --no-privileges --no-owner -h "$TARDIS_DB_HOST" -U "$TARDIS_DB_USER" -d "$TARDIS_DB_NAME" "$DB_DUMP_PATH" > "$LOG" 2>&1


for tbl in `PGPASSWORD=$PG_ADMIN_PASSWORD psql -qAt -h "$TARDIS_DB_HOST" -U "$PG_ADMIN_USER" -d "$TARDIS_DB_NAME" -c "select tablename from pg_tables where schemaname = 'public';"` ; do
    PGPASSWORD=$PG_ADMIN_PASSWORD psql -h "$TARDIS_DB_HOST" -U "$PG_ADMIN_USER" -d "$TARDIS_DB_NAME" -c "alter table \"$tbl\" owner to $TARDIS_DB_USER"
done


grep -v -q "SSL SYSCALL error: EOF detected" "$LOG"

exit $?
