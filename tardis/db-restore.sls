include:
  - tardis.db-migrate

{% set pg_admin_user = pillar['postgresql_admin_credentials']['user'] %}
{% set pg_admin_password = pillar['postgresql_admin_credentials']['password'] %}
{% set tardis_db_user = salt['pillar.get']('tardis:db_user', 'mytardis-'+grains['deployment']) %}
{% set tardis_db_password = salt['pillar.get']('tardis:db_password', 'myt4rd15') %}
{% set tardis_db_name = salt['pillar.get']('tardis:db_name', 'mytardis-'+grains['deployment']) %}

{% if salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound') %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% else %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_server_master', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% endif %}

# Ensure this state only affects the first node of web workers
{% if salt['mine.get']('G@roles:www and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items()[0][1][0] == salt['network.interfaces']()['eth0']['inet'][0]['address'] %}

# Do a db restore before migration if db dump path specified
{% set db_dump_path = salt['pillar.get']('tardis:restore_from_db_dump_path', None) %}
{% if db_dump_path %}

db_restore_script:
  file.managed:
    - name: /home/mytardis/db_restore.sh
    - source: salt://tardis/templates/db_restore.sh
    - template: jinja
    - mode: 755

db_restore:
  cmd.run:
    - name: count=0; until /home/mytardis/db_restore.sh; do ((count++)); sleep 5; if [ $count -gt 10 ]; then exit 1; fi; done
    - creates: /home/mytardis/db_restore.log
    - shell: /bin/bash
    - user: root
    - env:
        - PG_ADMIN_USER: {{ pg_admin_user }}
        - PG_ADMIN_PASSWORD: {{ pg_admin_password }}
        - TARDIS_DB_PASSWORD: {{ tardis_db_password }}
        - TARDIS_DB_HOST: {{ tardis_db_host }}
        - TARDIS_DB_USER: {{ tardis_db_user }}
        - TARDIS_DB_NAME: {{ tardis_db_name }}
        - DB_DUMP_PATH: {{ db_dump_path }}
    - require_in:
        - cmd: tardis_db_migrate
    - require:
        - postgres_database: tardis_db
        - file: db_restore_script

{% endif %}
{% endif %}