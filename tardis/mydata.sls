include:
  - tardis.base

/home/mytardis/mytardis/tardis/apps/mydata:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

mydata-app:
  git:
    - name: https://github.com/mytardis/mytardis-app-mydata.git
    - user: mytardis
    - latest
    - target: /home/mytardis/mytardis/tardis/apps/mydata
    - force_clone: True
    - force_checkout: True
    - force_reset: True
    - require:
      - file: /home/mytardis/mytardis/tardis/apps/mydata
