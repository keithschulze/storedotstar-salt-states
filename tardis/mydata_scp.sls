include:
  - tardis.base

mydata-group:
  group:
    - name: mydata
    - present

mydata-user:
  user:
    - name: mydata
    - present
    - shell: /bin/bash
    - groups:
        - mydata
        - mytardis
    - home: /home/mydata
    - require:
        - group: mytardis-group
        - group: mydata-group

/home/mydata/.ssh:
  file:
    - directory
    - user: mydata
    - group: mydata
    - mode: 700
    - require:
      - user: mydata-user

/home/mydata/.ssh/authorized_keys:
  file:
    - managed
    - user: mydata
    - group: mydata
    - mode: 600
    - require:
      - file: /home/mydata/.ssh

# /etc/ssh/moduli:
#   file.managed:
#     - mode: 600
#     - contents_pillar: mydata_scp:moduli

/etc/ssh/ssh_host_dsa_key:
  file.managed:
    - mode: 600
    - contents_pillar: mydata_scp:ssh_host_dsa_key

# /etc/ssh/ssh_host_dsa_key.pub:
#   file.managed:
#     - mode: 600
#     - contents_pillar: mydata_scp:ssh_host_dsa_key.pub

/etc/ssh/ssh_host_ecdsa_key:
  file.managed:
    - mode: 600
    - contents_pillar: mydata_scp:ssh_host_ecdsa_key

# /etc/ssh/ssh_host_ecdsa_key.pub:
#   file.managed:
#     - mode: 600
#     - contents_pillar: mydata_scp:ssh_host_ecdsa_key.pub

/etc/ssh/ssh_host_ed25519_key:
  file.managed:
    - mode: 600
    - contents_pillar: mydata_scp:ssh_host_ed25519_key

# /etc/ssh/ssh_host_ed25519_key.pub:
#   file.managed:
#     - mode: 600
#     - contents_pillar: mydata_scp:ssh_host_ed25519_key.pub

# /etc/ssh/ssh_host_key:
#   file.managed:
#     - mode: 600
#     - contents_pillar: mydata_scp:ssh_host_key
#
# /etc/ssh/ssh_host_key.pub:
#   file.managed:
#     - mode: 600
#     - contents_pillar: mydata_scp:ssh_host_key.pub

/etc/ssh/ssh_host_rsa_key:
  file.managed:
    - mode: 600
    - contents_pillar: mydata_scp:ssh_host_rsa_key

# /etc/ssh/ssh_host_rsa_key.pub:
#   file.managed:
#     - mode: 600
#     - contents_pillar: mydata_scp:ssh_host_rsa_key.pub
