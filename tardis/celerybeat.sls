include:
  - tardis.base

/etc/supervisor/conf.d/celerybeat.conf:
  file:
    - managed
    - source: salt://tardis/templates/celerybeat.conf
    - require:
        - pkg: supervisor
