/home/ubuntu/.ssh/authorized_keys:
  file.append:
    - text:
{% for key in salt['pillar.get']('admin:keys', []) %}
      - {{ key }}
{% endfor %}